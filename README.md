<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project ae.ae V0.3.90 -->
<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.tpl_namespace_root V0.3.12 -->
# db_ora 0.3.7

[![GitLab develop](https://img.shields.io/gitlab/pipeline/ae-group/ae_db_ora/develop?logo=python)](
    https://gitlab.com/ae-group/ae_db_ora)
[![LatestPyPIrelease](
    https://img.shields.io/gitlab/pipeline/ae-group/ae_db_ora/release0.3.6?logo=python)](
    https://gitlab.com/ae-group/ae_db_ora/-/tree/release0.3.6)
[![PyPIVersions](https://img.shields.io/pypi/v/ae_db_ora)](
    https://pypi.org/project/ae-db-ora/#history)

>ae namespace module portion db_ora: database system core layer to access Oracle databases.

[![Coverage](https://ae-group.gitlab.io/ae_db_ora/coverage.svg)](
    https://ae-group.gitlab.io/ae_db_ora/coverage/index.html)
[![MyPyPrecision](https://ae-group.gitlab.io/ae_db_ora/mypy.svg)](
    https://ae-group.gitlab.io/ae_db_ora/lineprecision.txt)
[![PyLintScore](https://ae-group.gitlab.io/ae_db_ora/pylint.svg)](
    https://ae-group.gitlab.io/ae_db_ora/pylint.log)

[![PyPIImplementation](https://img.shields.io/pypi/implementation/ae_db_ora)](
    https://gitlab.com/ae-group/ae_db_ora/)
[![PyPIPyVersions](https://img.shields.io/pypi/pyversions/ae_db_ora)](
    https://gitlab.com/ae-group/ae_db_ora/)
[![PyPIWheel](https://img.shields.io/pypi/wheel/ae_db_ora)](
    https://gitlab.com/ae-group/ae_db_ora/)
[![PyPIFormat](https://img.shields.io/pypi/format/ae_db_ora)](
    https://pypi.org/project/ae-db-ora/)
[![PyPILicense](https://img.shields.io/pypi/l/ae_db_ora)](
    https://gitlab.com/ae-group/ae_db_ora/-/blob/develop/LICENSE.md)
[![PyPIStatus](https://img.shields.io/pypi/status/ae_db_ora)](
    https://libraries.io/pypi/ae-db-ora)
[![PyPIDownloads](https://img.shields.io/pypi/dm/ae_db_ora)](
    https://pypi.org/project/ae-db-ora/#files)


## installation


execute the following command to install the
ae.db_ora module
in the currently active virtual environment:
 
```shell script
pip install ae-db-ora
```

if you want to contribute to this portion then first fork
[the ae_db_ora repository at GitLab](
https://gitlab.com/ae-group/ae_db_ora "ae.db_ora code repository").
after that pull it to your machine and finally execute the
following command in the root folder of this repository
(ae_db_ora):

```shell script
pip install -e .[dev]
```

the last command will install this module portion, along with the tools you need
to develop and run tests or to extend the portion documentation. to contribute only to the unit tests or to the
documentation of this portion, replace the setup extras key `dev` in the above command with `tests` or `docs`
respectively.

more detailed explanations on how to contribute to this project
[are available here](
https://gitlab.com/ae-group/ae_db_ora/-/blob/develop/CONTRIBUTING.rst)


## namespace portion documentation

information on the features and usage of this portion are available at
[ReadTheDocs](
https://ae.readthedocs.io/en/latest/_autosummary/ae.db_ora.html
"ae_db_ora documentation").
